//
//  ShopItemsViewController.swift
//  UQO
//
//  Created by sarin on 24/12/2018.
//  Copyright © 2018 UQO. All rights reserved.
//

import UIKit
import BouncyLayout
import MBProgressHUD
import Kingfisher

protocol ItemAddingDelegate {
    func addItemToOrder(from cell: UICollectionViewCell)
}

class ShopItemsViewController: BaseViewController {
    var viewmodel: ShopItemsViewModel?
    private let cellsInRawCount = 2

    @IBOutlet var itemsCollectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        if viewmodel == nil{
            dismiss(animated: true, completion: nil)
        }
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        itemsCollectionView.collectionViewLayout = layout
        title = viewmodel?.shop?.name ?? ""
        updateData()
    }
    
    func updateData(){
        MBProgressHUD.showAdded(to: view, animated: true)
        viewmodel?.updateShopItems(completion: { (error) in
            MBProgressHUD.hide(for: self.view, animated: true)
            if error == nil{
                self.itemsCollectionView.reloadData()
            } else{
                self.errorService.showError(title: "Error",
                                            message: "An error occurred on the server. Please try again later.",
                                            from: self)
            }
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let details = segue.destination as? DetailsViewController,
            let indexpath = itemsCollectionView.indexPathsForSelectedItems?.first{
            let item = viewmodel?.shop?.items[indexpath.row]
            details.details = Details(id: item?.itemId,
                                      name: item?.name,
                                      description: item?.itemDescription,
                                      url: item?.imageUrl)
            details.showAddButton = true
            details.delegate = self
            itemsCollectionView.deselectItem(at: indexpath, animated: true)
        }
    }
}

extension ShopItemsViewController: DetailsViewControllerDelegate{
    func add(id: String) {
        for item in (viewmodel?.shop?.items)!{
            if item.itemId == id{
                UserManager.sharedInstance.addToOrder(item: item)
                return
            }
        }
    }
}

extension ShopItemsViewController: UICollectionViewDelegate,
UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewmodel!.shop?.items.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ItemCollectionViewCell", for: indexPath) as! ItemCollectionViewCell
        if let item = viewmodel?.shop?.items[indexPath.row]{
            cell.configure(item: item)
            cell.delegate = self
        }
       
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.size.width / CGFloat(cellsInRawCount), height: 150)
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let item = cell as? ItemCollectionViewCell {
            item.imageView.kf.cancelDownloadTask()
        }
    }
}

extension ShopItemsViewController: ItemAddingDelegate{
    func addItemToOrder(from cell: UICollectionViewCell) {
        let hud = MBProgressHUD.showAdded(to: view, animated: true)
        hud.customView = UIImageView(image: UIImage(named: "items_complete"))
        hud.customView?.frame = CGRect(x: 0, y: 0, width: 65, height: 65)
        hud.mode = .customView
        hud.isUserInteractionEnabled = true
        hud.hide(animated: true, afterDelay: 0.9)
        let indexpath = itemsCollectionView.indexPath(for: cell)
        if let item = viewmodel?.shop?.items[(indexpath?.row)!]{
            add(id: item.itemId)
        }
    }
}
