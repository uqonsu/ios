//
//  ShopItemsViewModel.swift
//  UQO
//
//  Created by sarin on 25/12/2018.
//  Copyright © 2018 UQO. All rights reserved.
//

import UIKit

class ShopItemsViewModel {
    var shop : Shop?
    let shopService = ShopService()
    
    init(shop: Shop) {
        self.shop = shop
    }
    
    func updateShopItems(completion: @escaping (_: Error?) -> ()) {
        guard shop?.shopId != nil else{
            return
        }
        shopService.downloadShopItems(shopId: (shop?.shopId)!, completion: { (items, error) in
            if items != nil, error == nil{
                self.shop!.items = items!
            }
            completion(error)
        })
    }
}
