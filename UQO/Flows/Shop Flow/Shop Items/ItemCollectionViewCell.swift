//
//  ItemCollectionViewCell.swift
//  UQO
//
//  Created by sarin on 24/12/2018.
//  Copyright © 2018 UQO. All rights reserved.
//

import UIKit
import Kingfisher

class ItemCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var priceLabel: UILabel!
    var delegate : ItemAddingDelegate?
    
    func configure(item : Item){
        nameLabel.text = item.name
        priceLabel.text = String(format: "%.2f", (item.price ?? 0) / 100.0) + " RUB"
        if let url = item.imageUrl{
            imageView.kf.setImage(with: url)
        } else{
            imageView.image = UIImage(named: "defaultRestaurantBack")
        }
    }
    
    @IBAction func addButtonTapped(_ sender: UIButton) {
        UIView.animate(withDuration: 0.2) {
            sender.transform = sender.transform.concatenating(CGAffineTransform(rotationAngle: CGFloat(Double.pi)))
        }
        delegate?.addItemToOrder(from: self)
    }
}
