//
//  ShopDetailsViewController.swift
//  UQO
//
//  Created by sarin on 24/12/2018.
//  Copyright © 2018 UQO. All rights reserved.
//

import UIKit
import Kingfisher
import MBProgressHUD

protocol DetailsViewControllerDelegate {
    func add(id: String)
}

class DetailsViewController: UIViewController {

    var details: Details?
    var delegate: DetailsViewControllerDelegate?
    
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var descriotionLabel: UITextView!
    @IBOutlet var blurView: UIVisualEffectView!
    @IBOutlet var addButton: UIButton!
    @IBOutlet var imageView: UIImageView!
    var showAddButton = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateData()
    }
    
    func updateData() {
        if let _ = details{
            if let name = details?.name{
                 nameLabel.text = name
            }
            if let description = details?.detailsDescription{
                 descriotionLabel.text = description
            }
            if let url = details?.imageUrl{
                 imageView.kf.setImage(with: url)
            }
            addButton.isHidden = !showAddButton
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIView.animate(withDuration: 0.5, delay: 0.5, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.blurView.alpha = 0.95
        }, completion: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        UIView.animate(withDuration: 0.15) {
            self.blurView.alpha = 0.0
        }
        super.viewWillDisappear(animated)
    }
    
    @IBAction func closeButtonTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addButtonPressed(_ sender: UIButton) {
        let hud = MBProgressHUD.showAdded(to: view, animated: true)
        hud.customView = UIImageView(image: UIImage(named: "items_complete"))
        hud.customView?.frame = CGRect(x: 0, y: 0, width: 65, height: 65)
        hud.mode = .customView
        hud.isUserInteractionEnabled = true
        hud.hide(animated: true, afterDelay: 0.9)
        
        UIView.animate(withDuration: 0.2) {
            sender.transform = sender.transform.concatenating(CGAffineTransform(rotationAngle: CGFloat(Double.pi)))
        }
        if let _ = delegate, let _ = details?.detailsId{
            delegate!.add(id: (details?.detailsId)!)
        }
    }
}
