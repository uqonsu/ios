//
//  ShopsListViewModel.swift
//  UQO
//
//  Created by sarin on 25/12/2018.
//  Copyright © 2018 UQO. All rights reserved.
//

import UIKit

class ShopsListViewModel: Any {
    let shopService = ShopService()
    var shops = [Shop]()
    
    func updateShopsList(completion: @escaping (_: Error?) -> ()) {
        shopService.downloadShops { (shops, error) in
            if shops != nil, error == nil{
                self.shops = shops!
            }
            completion(error)
        }
    }
}
