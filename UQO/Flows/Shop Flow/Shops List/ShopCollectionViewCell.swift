//
//  ShopCollectionViewCell.swift
//  UQO
//
//  Created by sarin on 24/12/2018.
//  Copyright © 2018 UQO. All rights reserved.
//

import UIKit
import Kingfisher

protocol ShopCollectionViewCellDelegate {
    func showShopDetails(from cell: UICollectionViewCell)
}

class ShopCollectionViewCell: UICollectionViewCell {
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var shopImageView: UIImageView!
    var delegate: ShopCollectionViewCellDelegate?
    
    func configure(shop: Shop){
        nameLabel.text = shop.name
        descriptionLabel.text = shop.shopDescription
        if let url = shop.imageUrl{
            shopImageView.kf.setImage(with: url)
        } else{
            shopImageView.image = UIImage(named: "defaultRestaurantBack")
        }
    }
    
    @IBAction func shopDetailsButtonTapped(_ sender: Any) {
        delegate?.showShopDetails(from: self)
    }
}
