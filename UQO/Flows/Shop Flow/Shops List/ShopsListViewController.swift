//
//  ShopsListViewController.swift
//  UQO
//
//  Created by sarin on 24/12/2018.
//  Copyright © 2018 UQO. All rights reserved.
//

import UIKit
import BouncyLayout

class ShopsListViewController: BaseViewController{
    @IBOutlet var shopsCollectionView: UICollectionView!
    private let viewmodel = ShopsListViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let layout = BouncyLayout(style: BouncyLayout.BounceStyle.prominent)
        layout.minimumLineSpacing = 5
        shopsCollectionView.collectionViewLayout = layout
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateData()
    }
    
    private func updateData(){
        shopsCollectionView.reloadData()
        viewmodel.updateShopsList { (error) in
            if error == nil{
                self.shopsCollectionView.reloadData()
            } else{
                self.errorService.showError(title: "Error",
                                            message: "An error occurred on the server. Please try again later.",
                                            from: self)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? ShopItemsViewController, let indexpath = shopsCollectionView.indexPathsForSelectedItems?.first{
            let shop = viewmodel.shops[indexpath.row]
            destination.viewmodel = ShopItemsViewModel(shop: shop)
            shopsCollectionView.deselectItem(at: indexpath, animated: true)
        }
    }
}

extension ShopsListViewController: ShopCollectionViewCellDelegate{
    func showShopDetails(from cell: UICollectionViewCell) {
        if let indexpath = shopsCollectionView.indexPath(for: cell){
            if let details = self.storyboard?.instantiateViewController(withIdentifier: "DetailsViewController") as? DetailsViewController{
                details.modalPresentationStyle = .overFullScreen
                details.modalTransitionStyle = .flipHorizontal
                let shop = viewmodel.shops[indexpath.row]
                details.details = Details(id: nil,
                                      name: shop.name ?? "",
                                          description: shop.shopDescription,
                                          url: shop.imageUrl)
                details.showAddButton = false
                present(details, animated: true, completion: nil)
            }
        }
    }
}

extension ShopsListViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewmodel.shops.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ShopCollectionViewCell", for: indexPath) as! ShopCollectionViewCell
        cell.configure(shop: viewmodel.shops[indexPath.row])
        cell.delegate = self
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.size.width, height: 110)
    }
    
}
