//
//  AuthCodeViewController.swift
//  UQO
//
//  Created by sarin on 24/12/2018.
//  Copyright © 2018 UQO. All rights reserved.
//

import UIKit
import MBProgressHUD

class AuthCodeViewController: UIViewController {

    @IBOutlet var codeTextField: UITextField!
     private let authService = AuthService()

    @IBAction func verifyButtonTapped(_ sender: Any) {
        if let code = codeTextField.text, code.count > 0 {
            MBProgressHUD.showAdded(to: view, animated: true)
            authService.verify(code: code){ (error) in
                MBProgressHUD.hide(for: self.view, animated: true)
                if error == nil{
                    Interactor.sharedInstance.showMainFlow()
                }
            }
        }
    }
    
}
