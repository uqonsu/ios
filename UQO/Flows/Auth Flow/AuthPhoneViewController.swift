//
//  AuthPhoneViewController.swift
//  UQO
//
//  Created by sarin on 24/12/2018.
//  Copyright © 2018 UQO. All rights reserved.
//

import UIKit
import MBProgressHUD

class AuthPhoneViewController: UIViewController {

    @IBOutlet var phoneTextField: UITextField!
    private let authService = AuthService()
    
    @IBAction func getCodeButtonTapped(_ sender: Any) {
        if let phone = phoneTextField.text, phone.count > 0 {
            MBProgressHUD.showAdded(to: view, animated: true)
            authService.auth(phone: phone) { (error) in
                MBProgressHUD.hide(for: self.view, animated: true)
                if error == nil{
                    if let codeViewController = self.storyboard?.instantiateViewController(withIdentifier: "AuthCodeViewController"){
                        self.show(codeViewController, sender: self)
                    }
                }
            }
        }
    }
}
