//
//  ProfileViewController.swift
//  UQO
//
//  Created by sarin on 25/12/2018.
//  Copyright © 2018 UQO. All rights reserved.
//

import UIKit
import Stripe

class ProfileViewController: UIViewController {
    
    var paymentContext: STPPaymentContext!
    private let paymentService = PaymentService()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let customerContext = STPCustomerContext(keyProvider:paymentService)
        paymentContext = STPPaymentContext(customerContext: customerContext)
        paymentContext.hostViewController = self
    }
    

    @IBAction func logoutButtonTapped(_ sender: Any) {
        Interactor.sharedInstance.logout()
    }
    
    @IBAction func cardsInfoButtonprssed(_ sender: Any) {
        paymentContext.presentPaymentMethodsViewController()
    }
}


extension ProfileViewController: STPPaymentContextDelegate{
    func paymentContext(_ paymentContext: STPPaymentContext, didFailToLoadWithError error: Error) {
        
    }
    
    func paymentContextDidChange(_ paymentContext: STPPaymentContext) {
        
    }
    
    func paymentContext(_ paymentContext: STPPaymentContext, didCreatePaymentResult paymentResult: STPPaymentResult, completion: @escaping STPErrorBlock) {
        
    }
    
    func paymentContext(_ paymentContext: STPPaymentContext, didFinishWith status: STPPaymentStatus, error: Error?) {
        
    }
}
