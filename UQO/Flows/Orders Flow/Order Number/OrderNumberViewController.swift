//
//  OrderNumberViewController.swift
//  UQO
//
//  Created by sarin on 17/01/2019.
//  Copyright © 2019 UQO. All rights reserved.
//

import UIKit

class OrderNumberViewController: UIViewController {
    @IBOutlet private var nameLabel: UILabel!
    @IBOutlet private var paidLabel: UILabel!
    
    var orderName = ""
    var orderPaid = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameLabel.text = orderName
        if orderPaid{
            self.paidLabel.text = "PAID"
            self.paidLabel.textColor = UIColor(named: "Green Color")
        } else{
            self.paidLabel.text = "NOT PAID"
            self.paidLabel.textColor = UIColor(named: "Red Color")
        }
    }
    
    @IBAction func okButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

}
