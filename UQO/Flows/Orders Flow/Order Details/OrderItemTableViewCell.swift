//
//  OrderItemTableViewCell.swift
//  UQO
//
//  Created by sarin on 16/01/2019.
//  Copyright © 2019 UQO. All rights reserved.
//

import UIKit
import Kingfisher

class OrderItemTableViewCell: UITableViewCell {
    @IBOutlet private var nameLabel: UILabel!
    @IBOutlet private var priceLabel: UILabel!
    @IBOutlet private var itemImageView: UIImageView!
    
    func configure(item: Item?) {
        guard item != nil else {
            return
        }
        nameLabel.text = item!.name
        priceLabel.text = String(format: "%.2f", (item!.price ?? 0) / 100.0) + " RUB"
        if let url = item?.imageUrl{
            itemImageView.kf.setImage(with: url)
        }
    }
    
    

}
