//
//  OrderDetailsViewModel.swift
//  UQO
//
//  Created by sarin on 25/12/2018.
//  Copyright © 2018 UQO. All rights reserved.
//

import UIKit

class OrderDetailsViewModel: Any {
    private let ordersService = OrdersService()
    var order: Order
    let paymentService = PaymentService()
    
    init(order: Order) {
        self.order = order
    }
    
    func removeItem(at index: Int) {
        if index < order.items.count{
            ordersService.userManager.remove(from: order, at: index)
        }
    }
    
    func amountPrice() -> Double{
        var amount = 0.0
        for item in order.items {
            amount += item.price ?? 0
        }
        return amount
    }
    
    func pay(completion: @escaping (_: Error?) -> ()) {
        if let orderId = order.orderId{
            paymentService.pay(orderId: orderId) { (error) in
                if error == nil{
                    self.order.paid = true
                }
                completion(error)
            }
        }
    }
    
    func checkout(completion: @escaping (_: Error?) -> ()) {
        ordersService.create(order: order) { (order, error) in
            if error == nil, order != nil{
                
                for item in self.order.items{
                    order?.add(item)
                }
                self.order = order!
            }
            completion(error)
        }
    }
    
    func cancel(completion: @escaping (_: Error?) -> ()) {
        if let orderId = order.orderId{
            ordersService.cancel(orderId: orderId) { (error) in
                if error == nil{
                    self.order.status = .cancelled
                }
                completion(error)
            }
        }
    }
}
