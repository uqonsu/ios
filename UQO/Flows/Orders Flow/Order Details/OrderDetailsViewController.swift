//
//  OrderDetailsViewController.swift
//  UQO
//
//  Created by sarin on 25/12/2018.
//  Copyright © 2018 UQO. All rights reserved.
//

import UIKit
import MBProgressHUD

class OrderDetailsViewController: BaseViewController {
    
    var viewmodel: OrderDetailsViewModel?

    @IBOutlet var itemsTableView: UITableView!
    @IBOutlet var amountPriceLabel: UILabel!
    @IBOutlet var pickUpButton: UIButton!
    @IBOutlet var checkoutButton: UIButton!
    @IBOutlet var cancelButton: UIButton!
    @IBOutlet var payButton: UIButton!
    @IBOutlet var priceLabel: UILabel!
    @IBOutlet var stackview: UIStackView!
    
    @IBOutlet var stackviewHeightConstrint: NSLayoutConstraint!
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        itemsTableView.reloadData()
        updateControls(animated: false)
    }
    
    func updateControls(animated: Bool) {
        UIView.animate(withDuration: animated ? 0.3 : 0.0) {
            self.priceLabel.text = String(format: "%.2f", (self.viewmodel?.amountPrice() ?? 0) / 100.0) + " RUB"
            self.pickUpButton.isHidden = !(self.viewmodel?.order.status == Status.ready)
            self.checkoutButton.isHidden = !(self.viewmodel?.order.status == Status.local)
            let payEnabled = [Status.pending, Status.confirmed, Status.ready]
            self.cancelButton.isHidden = !payEnabled.contains(self.viewmodel?.order.status ?? .local)
            self.payButton.isHidden = !(payEnabled.contains(self.viewmodel?.order.status ?? .local) && !(self.viewmodel?.order.paid ?? false))
            var enable = false
            for view in self.stackview.arrangedSubviews{
                enable = enable || !view.isHidden
            }
            self.stackviewHeightConstrint.constant = enable ? 60 : 0
            self.title = self.viewmodel?.order.name ?? ""
            self.view.layoutIfNeeded()
        }
        if self.viewmodel?.order.items.count == 0 {
            navigationController?.popViewController(animated: true)
        }
    }

    @IBAction func payButtonPressed(_ sender: Any) {
        MBProgressHUD.showAdded(to: view, animated: true)
        viewmodel?.pay(completion: { (error) in
            DispatchQueue.main.async {
                MBProgressHUD.hide(for: self.view, animated: true)
                if error == nil{
                    self.updateControls(animated: true)
                } else{
                    self.errorService.showError(title: "Error",
                                                message: "An error occurred on the server. Please try again later.",
                                                from: self)
                }
            }
        })
    }
    
    @IBAction func checkoutButtonPressed(_ sender: Any) {
        MBProgressHUD.showAdded(to: view, animated: true)
        viewmodel?.checkout(completion: { (error) in
            DispatchQueue.main.async {
                MBProgressHUD.hide(for: self.view, animated: true)
                if error == nil{
                     self.updateControls(animated: true)
                } else{
                    self.errorService.showError(title: "Error",
                                                message: "An error occurred on the server. Please try again later.",
                                                from: self)
                }
            }
        })
    }
    
    @IBAction func cancelButtonPressed(_ sender: Any) {
        MBProgressHUD.showAdded(to: view, animated: true)
        viewmodel?.cancel(completion: { (error) in
            DispatchQueue.main.async {
                MBProgressHUD.hide(for: self.view, animated: true)
                if error == nil{
                     self.updateControls(animated: true)
                } else{
                    self.errorService.showError(title: "Error",
                                                message: "An error occurred on the server. Please try again later.",
                                                from: self)
                }
            }
        })
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let number = segue.destination as? OrderNumberViewController{
            number.orderPaid = viewmodel?.order.paid ?? false
            number.orderName = viewmodel?.order.name ?? ""
        }
    }
    
}

extension OrderDetailsViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewmodel?.order.items.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderItemTableViewCell") as! OrderItemTableViewCell
        cell.configure(item: viewmodel?.order.items[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return viewmodel?.order.status == Status.local
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete{
            viewmodel?.removeItem(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
            updateControls(animated: true)
        }
    }
}
