//
//  OrdersHistoryViewModel.swift
//  UQO
//
//  Created by sarin on 24/12/2018.
//  Copyright © 2018 UQO. All rights reserved.
//

import UIKit

class OrdersHistoryViewModel: Any {
    private var historyOrders = [Order]()
    private let ordersService = OrdersService()
    
    func updateHistoryOrders(completion: @escaping (_: Error?) -> ()) {
        ordersService.downloadOrders { (orders, error) in
            if orders != nil, error == nil{
                self.historyOrders = orders!
            }
            completion(error)
        }
    }
    
    func currentOrders() -> [Order]{
        return ordersService.currentOrders()
    }
    
    func sortedHistoryOrders() -> [Order]{
        return historyOrders.sorted(by: { $0.date?.timeIntervalSince1970 ?? 0 > $1.date?.timeIntervalSince1970 ?? 0 })
    }
}
