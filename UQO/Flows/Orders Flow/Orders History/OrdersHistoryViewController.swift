//
//  OrdersHistoryViewController.swift
//  UQO
//
//  Created by sarin on 24/12/2018.
//  Copyright © 2018 UQO. All rights reserved.
//

import UIKit
import MBProgressHUD

class OrdersHistoryViewController: BaseViewController {

    @IBOutlet var ordersTableView: UITableView!
    let refreshControl = UIRefreshControl()
    let viewmodel = OrdersHistoryViewModel()
    
    private let sectionTitles = ["Current", "History"]
    override func viewDidLoad() {
        super.viewDidLoad()
        refreshControl.addTarget(self, action: #selector(updateData), for: UIControl.Event.valueChanged)
        ordersTableView.addSubview(refreshControl)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        ordersTableView.reloadData()
        updateData()
    }
    
    @objc func updateData(){
        if viewmodel.sortedHistoryOrders().count == 0{
            MBProgressHUD.showAdded(to: view, animated: true)
        }
        viewmodel.updateHistoryOrders { (error) in
            MBProgressHUD.hide(for: self.view, animated: true)
            self.refreshControl.endRefreshing()
            if error == nil{
                self.ordersTableView.reloadData()
            } else{
                self.errorService.showError(title: "Error",
                                       message: "An error occurred on the server. Please try again later.",
                                       from: self)
            }
        }
    }
    
    func order(indexPath: IndexPath) -> Order?{
        switch indexPath.section {
        case 0:
            return viewmodel.currentOrders()[indexPath.row]
        case 1:
            return viewmodel.sortedHistoryOrders()[indexPath.row]
        default:
            return nil
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let details = segue.destination as? OrderDetailsViewController,
            let indexpath = self.ordersTableView.indexPathForSelectedRow{
            details.viewmodel = OrderDetailsViewModel(order: self.order(indexPath: indexpath)!)
        }
    }
}

extension OrdersHistoryViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        view.backgroundColor = UIColor.clear
        view.tintColor = UIColor.clear
        if let header = view as? UITableViewHeaderFooterView{
            header.textLabel?.textColor = UIColor.black
            header.textLabel?.font = UIFont(name: "HelveticaNeue-Bold", size: 19)
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionTitles.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sectionTitles[section].uppercased()
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return viewmodel.currentOrders().count
        case 1:
            return viewmodel.sortedHistoryOrders().count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderTableViewCell") as! OrderTableViewCell
        cell.configure(order: self.order(indexPath: indexPath)!)
        return cell
    }
}
