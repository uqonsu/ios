//
//  OrderTableViewCell.swift
//  UQO
//
//  Created by sarin on 24/12/2018.
//  Copyright © 2018 UQO. All rights reserved.
//

import UIKit
import Kingfisher

class OrderTableViewCell: UITableViewCell {

    @IBOutlet var shopImageView: UIImageView!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var itemsLabel: UILabel!
    @IBOutlet var statusLabel: UILabel!
    @IBOutlet var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        nameLabel.transform = CGAffineTransform(rotationAngle: CGFloat(-Double.pi / 2))
    }
    
    func configure(order: Order){
        if let date = order.date{
            let dateFormatter : DateFormatter = DateFormatter()
            dateFormatter.dateFormat = "HH:mm"
            dateLabel.text = dateFormatter.string(from: date)
        } else{
            dateLabel.text = ""
        }
        
        var items = ""
        var count = 1
        for item in order.items{
            if let name = item.name{
                items += "\(count). " + name + "\n"
                count += 1
            }
        }
        itemsLabel.text = items
        statusLabel.text = order.status.name().uppercased()
        nameLabel.text = order.name
        if let url = order.imageUrl{
            shopImageView.kf.setImage(with: url)
        } else if order.status == .local{
            shopImageView.image = nil
        } else{
            shopImageView.image = UIImage(named: "defaultRestaurantBack")
        }
    }
}
