//
//  DetailsObject.swift
//  UQO
//
//  Created by sarin on 18/01/2019.
//  Copyright © 2019 UQO. All rights reserved.
//

import UIKit

class Details {
    let detailsId: String?
    let detailsDescription: String?
    let name: String?
    let imageUrl: URL?
    
    init(id: String?, name: String?, description: String?, url: URL?) {
        self.detailsId = id
        self.detailsDescription = description
        self.name = name
        self.imageUrl = url
    }
}
