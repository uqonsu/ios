//
//  Order.swift
//  UQO
//
//  Created by sarin on 24/12/2018.
//  Copyright © 2018 UQO. All rights reserved.
//

import UIKit
import SwiftyJSON

enum Status : Int, CaseIterable {
    case local = 0
    case pending
    case confirmed
    case ready
    case expired
    case cancelled
    case error
    case completed
    
    func name() -> String{
        switch self {
        case .local:
            return "local"
        case .pending:
            return "pending"
        case .confirmed:
            return "confirmed"
        case .ready:
            return "ready"
        case .expired:
            return "expired"
        case .cancelled:
            return "cancelled"
        case .error:
            return "error"
        case .completed:
            return "completed"
        }
    }
}

class Order{
    var orderId : String?
    var organizationId : String?
    private(set) var items = [Item]()
    var status = Status.local
    var date : Date?
    var paid = false
    var name = ""
    var imageUrl: URL?
    
    init(organizationId: String) {
        self.organizationId = organizationId
        orderId = UUID.init().uuidString
    }
    
    func add(_ item: Item){
        items.append(item)
    }
    
    func remove(at index: Int){
        items.remove(at: index)
    }
    
    init(json: JSON) {
        orderId = json["id"].string
        organizationId = json["shop_id"].string
        var items = [Item]()
        for itemJson in json["items"].arrayValue{
            items.append(Item(json: itemJson))
        }
        self.items = items
        status = Status(rawValue: json["status"].int ?? 0) ?? .pending
        
        let dateString = json["order_time"].string ?? ""
        let dateFormatter : DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-DD'T'HH:mm:ss"
        date = dateFormatter.date(from: dateString)
        paid = json["order_paid"].bool ?? false
        name = json["order_name"].string ?? ""
        if let urlString = json["image"]["medium"].string, urlString.count > 0{
            imageUrl = URL(string: urlString)
        }
    }
    
}
