//
//  Item.swift
//  UQO
//
//  Created by sarin on 24/12/2018.
//  Copyright © 2018 UQO. All rights reserved.
//

import UIKit
import SwiftyJSON

class Item{
    var itemId : String
    var organizationId : String
    var name : String?
    var itemDescription : String?
    var price : Double?
    var imageUrl: URL?
    
    init(json: JSON) {
        itemId = json["id"].string ?? ""
        organizationId = json["organization_id"].string ?? ""
        name = json["name"].string
        itemDescription = json["description"].string
        price = json["price"].double
        if let urlString = json["image"]["medium"].string, urlString.count > 0{
            imageUrl = URL(string: urlString)
        }
    }
    
    func json() -> JSON{
        return ["id" : itemId as Any,
                "organization_id" : organizationId as Any,
                "name" : name as Any,
                "price" : price as Any]
    }
}
