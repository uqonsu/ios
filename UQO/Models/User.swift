//
//  User.swift
//  UQO
//
//  Created by sarin on 24/12/2018.
//  Copyright © 2018 UQO. All rights reserved.
//

import UIKit

class User: NSObject {
    var phoneNumber = ""
    var firstName = "First"
    var lastName = "Last"
    var currentOrders = [Order]()
    
    func fullName() -> String {
        return firstName + " " + lastName
    }
}
