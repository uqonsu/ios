//
//  Restaurant.swift
//  UQO
//
//  Created by sarin on 24/12/2018.
//  Copyright © 2018 UQO. All rights reserved.
//

import UIKit
import SwiftyJSON

class Shop: Any {
    var shopId : String?
    var shopDescription : String?
    var organizationId : String?
    var name : String?
    var online : Date?
    var items = [Item]()
    var imageUrl:  URL?
    
    init(json: JSON) {
        shopId = json["id"].string
        shopDescription = json["description"].string
        organizationId = json["organization_id"].string
        name = json["organization_name"].string
        if let urlString = json["image"]["medium"].string, urlString.count > 0{
            imageUrl = URL(string: urlString)
        }
    }
}
