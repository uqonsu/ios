//
//  Interactor.swift
//  UQO
//
//  Created by sarin on 24/12/2018.
//  Copyright © 2018 UQO. All rights reserved.
//

import UIKit

class Interactor{
    
    private let userManager = UserManager.sharedInstance
    var rootViewController : UIViewController!{
        didSet{
            if userManager.token.count > 0{
                showMainFlow()
            } else{
                showAuthFlow()
            }
        }
    }
    
    static let sharedInstance = Interactor()
    
    private init(){
        
    }
    
    func hideAllFlows(){
        let children = rootViewController.children
        for child in children{
            child.view.removeFromSuperview()
            child.removeFromParent()
        }
    }
    
    func showAuthFlow() {
        let storyboard = UIStoryboard.init(name: "Auth", bundle: Bundle.main)
        let auth = storyboard.instantiateInitialViewController()!
        showFlow(root: auth)
    }
    
    func showMainFlow(){
        let storyboard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
        let tabbar = storyboard.instantiateViewController(withIdentifier: "MainFlowTabBarViewController")
        showFlow(root: tabbar)
    }
    
    func showFlow(root: UIViewController){
        hideAllFlows()
        rootViewController.addChild(root)
        rootViewController.view.addSubview(root.view)
        root.didMove(toParent: rootViewController)
    }
    
    func logout() {
        showAuthFlow()
        UserManager.sharedInstance.logout()
    }
}
