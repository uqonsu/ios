//
//  UqoError.swift
//  UQO
//
//  Created by sarin on 24/12/2018.
//  Copyright © 2018 UQO. All rights reserved.
//

import UIKit

enum UqoError : Error {
    case AuthenticationFailed
    case VerificationFailed
    case JSONEncodingFailed
    case ResponseValidationFailed
}
