//
//  UserManager.swift
//  UQO
//
//  Created by sarin on 24/12/2018.
//  Copyright © 2018 UQO. All rights reserved.
//

import UIKit
import Alamofire

private let UserTokenKey = "com.uqo.UQO.user.token"

class UserManager{
    
    static let sharedInstance = UserManager()
    var verificationToken : String?
    var token = "" {
        didSet{
            verificationToken = ""
            UserDefaults.standard.set(token, forKey: UserTokenKey)
            if token.count > 0{
                 sessionManager.adapter = SecurityAdapter(accessToken: token)
            } else{
               sessionManager.adapter = nil
            }
        }
    }
    var user = User()
    
    let baseURL = URL(string: "http://benis.site:8000/")!
    var sessionManager: SessionManager = SessionManager.default
    
    private init() {
        token = UserDefaults.standard.object(forKey: UserTokenKey) as? String ?? ""
        if token.count > 0{
            sessionManager.adapter = SecurityAdapter(accessToken: token)
        }
    }
    
    func logout(){
        token = ""
    }
    
    
    
    func addToOrder(item: Item){
        for order in user.currentOrders {
            if order.organizationId == item.organizationId{
                order.add(item)
                return
            }
        }
        let order = Order(organizationId: item.organizationId)
        order.add(item)
        user.currentOrders.append(order)
    }
    
    func remove(from order: Order, at index: Int){
        order.remove(at: index)
        if order.items.count == 0{
            user.currentOrders.removeAll { (currentOrder) -> Bool in
                return currentOrder.orderId == order.orderId
            }
        }
    }
}

class SecurityAdapter: RequestAdapter {
    private let accessToken: String?
    
    init(accessToken: String?) {
        self.accessToken = accessToken
    }
    
    func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
        guard accessToken != nil else {
            return urlRequest
        }
        
        var urlRequest = urlRequest
        
        urlRequest.setValue("Bearer " + accessToken!, forHTTPHeaderField: "Authorization")
        
        return urlRequest
    }
}
