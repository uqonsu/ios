//
//  OrdersService.swift
//  UQO
//
//  Created by sarin on 24/12/2018.
//  Copyright © 2018 UQO. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class OrdersService: BaseService {
    
    func downloadOrders(completion: @escaping (_: [Order]?, _: Error?) -> ()) {
        let url = baseURL.appendingPathComponent("customer/order")
        
        sessionManager.request(url,
                               method: .get,
                               parameters: nil,
                               encoding: Alamofire.JSONEncoding.default,
                               headers: nil)
            .validate()
            .responseData { (response) in
                switch response.result {
                case .success(let data):
                    guard let json = try? JSON(data: data) else {
                        completion(nil, UqoError.JSONEncodingFailed)
                        return
                    }
                    var orders = [Order]()
                    for orderJson in json["orders"].arrayValue{
                        orders.append(Order(json: orderJson))
                    }
                    completion(orders, nil)
                case .failure(let error):
                    completion(nil, error)
                }
        }
    }
    
    func currentOrders() -> [Order]{
        return userManager.user.currentOrders
    }
    
    func create(order: Order, completion: @escaping (_ : Order?,_: Error?) -> ()) {
        let dateFormatter : DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = Date()
        let dateString = dateFormatter.string(from: date)
        
        let url = baseURL.appendingPathComponent("/order")
        var items = [Any]()
        for item in order.items {
            items.append(["id" : item.itemId,
                          "amount" : 1])
        }
        let parameters = ["shop_id": order.organizationId as Any,
                          "time": dateString,
                          "items": items] as [String : Any]
        sessionManager.request(url,
                               method: .post,
                               parameters: parameters,
                               encoding: Alamofire.JSONEncoding.default,
                               headers: nil)
            .validate()
            .responseData { (response) in
                switch response.result {
                case .success(let data):
                    guard let json = try? JSON(data: data) else {
                        completion(nil, UqoError.JSONEncodingFailed)
                        return
                    }
                    self.userManager.user.currentOrders.removeAll(where: { (currentOrder) -> Bool in
                        return currentOrder.orderId == order.orderId
                    })
                    completion(Order(json: json), nil)
                case .failure(_):
                    completion(nil, UqoError.ResponseValidationFailed)
                }
        }
    }
    
    func cancel(orderId: String, completion: @escaping (_: Error?) -> ()) {
        let url = baseURL.appendingPathComponent("order/" + orderId + "/cancel")
        
        sessionManager.request(url,
                               method: .post,
                               parameters: nil,
                               encoding: Alamofire.JSONEncoding.default,
                               headers: nil)
            .validate()
            .responseData { (response) in
                switch response.result {
                case .success(_):
                    completion(nil)
                case .failure(_):
                    completion(UqoError.ResponseValidationFailed)
                }
        }
    }
}
