//
//  PaymentService.swift
//  UQO
//
//  Created by sarin on 16/01/2019.
//  Copyright © 2019 UQO. All rights reserved.
//

import UIKit
import Stripe
import Alamofire

class PaymentService: BaseService, STPEphemeralKeyProvider {
    func createCustomerKey(withAPIVersion apiVersion: String, completion: @escaping STPJSONResponseCompletionBlock) {
        let url = baseURL.appendingPathComponent("customer/issue_key")
        sessionManager.request(url,
                               method: .post,
                               parameters: [ "stripe_version": apiVersion],
                               encoding: Alamofire.JSONEncoding.default,
                               headers: nil)
            .validate()
            .responseJSON { responseJSON in
                switch responseJSON.result {
                case .success(let json):
                    completion(json as? [String: AnyObject], nil)
                case .failure(let error):
                    completion(nil, error)
                }
        }
    }
    
    func pay(orderId: String, completion: @escaping (_: Error?) -> ()){
        let url = baseURL.appendingPathComponent("order/" + orderId + "/payment")
        sessionManager.request(url,
                               method: .post,
                               parameters: nil,
                               encoding: Alamofire.JSONEncoding.default,
                               headers: nil)
            .validate()
            .responseJSON { responseJSON in
                switch responseJSON.result {
                case .success( _):
                    completion(nil)
                case .failure(_):
                    completion(UqoError.ResponseValidationFailed)
                }
        }
    }
}
