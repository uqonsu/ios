//
//  ShopService.swift
//  UQO
//
//  Created by sarin on 25/12/2018.
//  Copyright © 2018 UQO. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ShopService: BaseService {

    func downloadShops(completion: @escaping (_: [Shop]?, _: Error?) -> ()) {
        let url = baseURL.appendingPathComponent("shop")
        
        sessionManager.request(url,
                               method: .get,
                               parameters: nil,
                               encoding: Alamofire.JSONEncoding.default,
                               headers: nil)
            .validate()
            .responseData { (response) in
                switch response.result {
                case .success(let data):
                    guard let json = try? JSON(data: data) else {
                        completion(nil, UqoError.JSONEncodingFailed)
                        return
                    }
                    var shops = [Shop]()
                    for shopJson in json["shops"].arrayValue{
                        shops.append(Shop(json: shopJson))
                    }
                    completion(shops, nil)
                case .failure(_):
                    completion(nil, UqoError.ResponseValidationFailed)
                }
        }
    }
    
    func downloadShopItems(shopId: String, completion: @escaping (_: [Item]?, _: Error?) -> ()) {
        let url = baseURL.appendingPathComponent("shop/" + shopId + "/items")
        
        sessionManager.request(url,
                               method: .get,
                               parameters: nil,
                               encoding: Alamofire.JSONEncoding.default,
                               headers: nil)
            .validate()
            .responseData { (response) in
                switch response.result {
                case .success(let data):
                    guard let json = try? JSON(data: data) else {
                        completion(nil, UqoError.JSONEncodingFailed)
                        return
                    }
                    var items = [Item]()
                    for itemJson in json["items"].arrayValue{
                        items.append(Item(json: itemJson))
                    }
                    completion(items, nil)
                case .failure(_):
                    completion(nil, UqoError.ResponseValidationFailed)
                }
        }
    }
    
}
