//
//  BaseService.swift
//  UQO
//
//  Created by sarin on 24/12/2018.
//  Copyright © 2018 UQO. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class BaseService: NSObject {
    let userManager = UserManager.sharedInstance
    var baseURL : URL
    var sessionManager : SessionManager
    
    override init() {
        sessionManager = userManager.sessionManager
        baseURL = userManager.baseURL
        super.init()
    }
}
