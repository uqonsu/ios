//
//  AuthService.swift
//  UQO
//
//  Created by sarin on 24/12/2018.
//  Copyright © 2018 UQO. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class AuthService: BaseService{
    
    func auth(phone: String, completion: @escaping (_: Error?) -> ()) {
        let url = baseURL.appendingPathComponent("customer/auth")
        let parameters = ["phone_number" : phone]
        
        sessionManager.request(url,
                               method: .post,
                               parameters: parameters,
                               encoding:Alamofire.JSONEncoding.default,
                               headers: nil)
            .validate()
            .responseData { (response) in
                switch response.result {
                case .success(let data):
                    guard let json = try? JSON(data: data) else {
                        completion(UqoError.JSONEncodingFailed)
                        return
                    }
                    let token = json["token"].string ?? ""
                    if token.count > 0{
                        self.userManager.verificationToken = token
                        completion(nil)
                    } else{
                        completion(UqoError.AuthenticationFailed)
                    }
                case .failure(let error):
                    completion(error)
                }
        }
    }
    
    func verify(code: String, completion: @escaping (_: Error?) -> ()) {
        let url = baseURL.appendingPathComponent("customer/verify")
        let parameters = ["token" : userManager.verificationToken!,
                          "code" : code] as [String : Any]
        sessionManager.request(url,
                               method: .post,
                               parameters: parameters,
                               encoding: Alamofire.JSONEncoding.default,
                               headers: nil)
            .validate()
            .responseData { (response) in
                switch response.result {
                case .success(let data):
                    guard let json = try? JSON(data: data) else {
                        completion(UqoError.JSONEncodingFailed)
                        return
                    }
                    let token = json["token"].string ?? ""
                    if token.count > 0{
                        self.userManager.token = token
                        completion(nil)
                    } else{
                        completion(UqoError.AuthenticationFailed)
                    }
                case .failure(let error):
                    completion(error)
                }
        }
    }
}
