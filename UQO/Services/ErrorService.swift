//
//  ErrorService.swift
//  UQO
//
//  Created by sarin on 19/01/2019.
//  Copyright © 2019 UQO. All rights reserved.
//

import UIKit

class ErrorService: Any {
    
    func showError(title: String?, message: String?, from vc: UIViewController) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        let ok = UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil)
        alert.addAction(ok)
        vc.present(alert, animated: true, completion: nil)
    }

}
