//
//  UIView+CustomFields.swift
//  UQO
//
//  Created by sarin on 24/12/2018.
//  Copyright © 2018 UQO. All rights reserved.
//

import UIKit

extension UIView{
    @IBInspectable var cornerRadius : CGFloat {
        set {
            layer.cornerRadius = newValue
        }
        
        get {
            return layer.cornerRadius
        }
    }
    
    @IBInspectable var shadowOffset : CGSize {
        set {
            layer.shadowOffset = newValue
        }
        
        get {
            return layer.shadowOffset
        }
    }
    
    @IBInspectable var shadowColor : UIColor {
        set {
            layer.shadowColor = newValue.cgColor
        }
        
        get {
            return UIColor(cgColor: layer.shadowColor ?? UIColor.clear.cgColor)
        }
    }
    
    @IBInspectable var borderColor : UIColor {
        set {
            layer.borderColor = newValue.cgColor
        }
        
        get {
            return UIColor(cgColor: layer.borderColor ?? UIColor.clear.cgColor)
        }
    }
    
    @IBInspectable var borderWidth : CGFloat {
        set {
            layer.borderWidth = newValue
        }
        
        get {
            return layer.borderWidth
        }
    }
    
    @IBInspectable var shadowOpacity : Float{
        set {
            layer.shadowOpacity = newValue
        }
        
        get {
            return layer.shadowOpacity
        }
    }
    
    @IBInspectable var shadowRadius : CGFloat{
        set {
            layer.shadowRadius = newValue
        }
        
        get {
            return layer.shadowRadius
        }
    }
    
}

